/**
* クラス名   : Core_AccountInfoDetailTest.cls
* クラス概要 : Apexテストクラス
* @created  : 2020/06/18 KSVC Nguyen The Phuong
* @modified :
*/
@IsTest
private class Core_AccountInfoDetailTest {

	private static User objUserTest;
	
	static {
		objUserTest = createUser();
	}

	/*
	* testMethodRestGetAccountDetail_1
	* @created: 2020/06/18 KSVC Nguyen The Phuong
	* @modified: 
	*/
	static testMethod void testMethodRestGetAccountDetail_1() {
		System.runAs(objUserTest) {
			String recordTypeCustomer = [SELECT Id FROM RecordType WHERE Name = 'Customer' OR DeveloperName = 'Customer' LIMIT 1].Id;
			Account objAccount = createAccount(recordTypeCustomer, true, true, true);
			String strAccountNumber = [SELECT JPN_AccountNumber__c FROM Account WHERE Id = :objAccount.Id].JPN_AccountNumber__c;
			Lead objLead = createLead(strAccountNumber);
			createCampaignMember(objLead.Id);
			createOpportunity(objAccount.Id, 'Phone');
			Product2 objProduct2 = createProduct2();
			createServiceContract(objAccount.Id, objProduct2.Id, 'Credit Card');

			RestRequest req = new RestRequest();
			RestResponse res = new RestResponse();
			req.requestURI = '/services/apexrest/AccountInfoDetail/11223344';
			req.httpMethod = 'GET';
			req.addHeader('Content-Type', 'application/json'); 
			RestContext.request = req;
			RestContext.response = res;
			Test.startTest();
			Core_AccountInfoDetail.getAccountDetail();
			Test.stopTest();

			Map<String, Object> mapResponse = (Map<String, Object>)JSON.deserializeUntyped(res.responseBody.toString());
			Map<String, Object> mapAccountInfo = (Map<String, Object>)mapResponse.get('objAccount');
			Map<String, Object> mapServiceContractInfo = (Map<String, Object>)mapResponse.get('objServiceContract');
			System.assertEquals(200, res.statusCode);
			System.assertEquals('両方', (String)mapAccountInfo.get('SendMethod__c'));
			System.assertEquals('Credit Card', (String)mapServiceContractInfo.get('CORE_Payment_Method__c'));
		}
	}

	/*
	* testMethodRestGetAccountDetail_2
	* @created: 2020/06/18 KSVC Nguyen The Phuong
	* @modified: 
	*/
	static testMethod void testMethodRestGetAccountDetail_2() {
		System.runAs(objUserTest) {
			String recordTypeCustomer = [SELECT Id FROM RecordType WHERE Name = 'Customer' OR DeveloperName = 'Customer' LIMIT 1].Id;
			Account objAccount = createAccount(recordTypeCustomer, true, true, false);
			String strAccountNumber = [SELECT JPN_AccountNumber__c FROM Account WHERE Id = :objAccount.Id].JPN_AccountNumber__c;
			Lead objLead = createLead(strAccountNumber);
			createCampaignMember(objLead.Id);
			createOpportunity(objAccount.Id, 'Website');
			Product2 objProduct2 = createProduct2();
			createServiceContract(objAccount.Id, objProduct2.Id, 'Direct Debit');

			RestRequest req = new RestRequest();
			RestResponse res = new RestResponse();
			req.requestURI = '/services/apexrest/AccountInfoDetail/11223344';
			req.httpMethod = 'GET';
			req.addHeader('Content-Type', 'application/json'); 
			RestContext.request = req;
			RestContext.response = res;
			Test.startTest();
			Core_AccountInfoDetail.getAccountDetail();
			Test.stopTest();
			
			Map<String, Object> mapResponse = (Map<String, Object>)JSON.deserializeUntyped(res.responseBody.toString());
			Map<String, Object> mapAccountInfo = (Map<String, Object>)mapResponse.get('objAccount');
			Map<String, Object> mapServiceContractInfo = (Map<String, Object>)mapResponse.get('objServiceContract');
			System.assertEquals(200, res.statusCode);
			System.assertEquals('両方', (String)mapAccountInfo.get('SendMethod__c'));
			System.assertEquals('Direct Debit', (String)mapServiceContractInfo.get('CORE_Payment_Method__c'));
		}
	}

	/*
	* testMethodRestGetAccountDetail_3
	* @created: 2020/06/18 KSVC Nguyen The Phuong
	* @modified: 
	*/
	static testMethod void testMethodRestGetAccountDetail_3() {
		System.runAs(objUserTest) {
			String recordTypeCustomer = [SELECT Id FROM RecordType WHERE Name = 'Customer' OR DeveloperName = 'Customer' LIMIT 1].Id;
			Account objAccount = createAccount(recordTypeCustomer, true, false, false);
			String strAccountNumber = [SELECT JPN_AccountNumber__c FROM Account WHERE Id = :objAccount.Id].JPN_AccountNumber__c;
			Lead objLead = createLead(strAccountNumber);
			createCampaignMember(objLead.Id);
			createOpportunity(objAccount.Id, null);
			Product2 objProduct2 = createProduct2();
			createServiceContract(objAccount.Id, objProduct2.Id, null);

			RestRequest req = new RestRequest();
			RestResponse res = new RestResponse();
			req.requestURI = '/services/apexrest/AccountInfoDetail/11223344';
			req.httpMethod = 'GET';
			req.addHeader('Content-Type', 'application/json'); 
			RestContext.request = req;
			RestContext.response = res;
			Test.startTest();
			Core_AccountInfoDetail.getAccountDetail();
			Test.stopTest();
			
			Map<String, Object> mapResponse = (Map<String, Object>)JSON.deserializeUntyped(res.responseBody.toString());
			Map<String, Object> mapAccountInfo = (Map<String, Object>)mapResponse.get('objAccount');
			Map<String, Object> mapServiceContractInfo = (Map<String, Object>)mapResponse.get('objServiceContract');
			System.assertEquals(200, res.statusCode);
			System.assertEquals('電子メール', (String)mapAccountInfo.get('SendMethod__c'));
			System.assertEquals(null, (String)mapServiceContractInfo.get('CORE_Payment_Method__c'));
		}
	}

	/*
	* testMethodRestGetAccountDetail_4
	* @created: 2020/06/18 KSVC Nguyen The Phuong
	* @modified: 
	*/
	static testMethod void testMethodRestGetAccountDetail_4() {
		System.runAs(objUserTest) {
			String recordTypeCustomer = [SELECT Id FROM RecordType WHERE Name = 'Customer' OR DeveloperName = 'Customer' LIMIT 1].Id;
			Account objAccount = createAccount(recordTypeCustomer, false, true, false);
			String strAccountNumber = [SELECT JPN_AccountNumber__c FROM Account WHERE Id = :objAccount.Id].JPN_AccountNumber__c;
			Lead objLead = createLead(strAccountNumber);
			createCampaignMember(objLead.Id);
			createOpportunity(objAccount.Id, null);
			Product2 objProduct2 = createProduct2();
			createServiceContract(objAccount.Id, objProduct2.Id, null);

			RestRequest req = new RestRequest();
			RestResponse res = new RestResponse();
			req.requestURI = '/services/apexrest/AccountInfoDetail/11223344';
			req.httpMethod = 'GET';
			req.addHeader('Content-Type', 'application/json'); 
			RestContext.request = req;
			RestContext.response = res;
			Test.startTest();
			Core_AccountInfoDetail.getAccountDetail();
			Test.stopTest();
			
			Map<String, Object> mapResponse = (Map<String, Object>)JSON.deserializeUntyped(res.responseBody.toString());
			Map<String, Object> mapAccountInfo = (Map<String, Object>)mapResponse.get('objAccount');
			Map<String, Object> mapServiceContractInfo = (Map<String, Object>)mapResponse.get('objServiceContract');
			System.assertEquals(200, res.statusCode);
			System.assertEquals('郵送', (String)mapAccountInfo.get('SendMethod__c'));
			System.assertEquals(null, (String)mapServiceContractInfo.get('CORE_Payment_Method__c'));
		}
	}

	/*
	* testMethodRestGetAccountDetail_5
	* @created: 2020/06/18 KSVC Nguyen The Phuong
	* @modified: 
	*/
	static testMethod void testMethodRestGetAccountDetail_5() {
		System.runAs(objUserTest) {
			String recordTypeCustomer = [SELECT Id FROM RecordType WHERE Name = 'Customer' OR DeveloperName = 'Customer' LIMIT 1].Id;
			Account objAccount = createAccount(recordTypeCustomer, false, false, false);
			String strAccountNumber = [SELECT JPN_AccountNumber__c FROM Account WHERE Id = :objAccount.Id].JPN_AccountNumber__c;
			Lead objLead = createLead(strAccountNumber);
			createCampaignMember(objLead.Id);
			createOpportunity(objAccount.Id, null);
			Product2 objProduct2 = createProduct2();
			createServiceContract(objAccount.Id, objProduct2.Id, null);

			RestRequest req = new RestRequest();
			RestResponse res = new RestResponse();
			req.requestURI = '/services/apexrest/AccountInfoDetail/11223344';
			req.httpMethod = 'GET';
			req.addHeader('Content-Type', 'application/json'); 
			RestContext.request = req;
			RestContext.response = res;
			Test.startTest();
			Core_AccountInfoDetail.getAccountDetail();
			Test.stopTest();
			
			Map<String, Object> mapResponse = (Map<String, Object>)JSON.deserializeUntyped(res.responseBody.toString());
			Map<String, Object> mapAccountInfo = (Map<String, Object>)mapResponse.get('objAccount');
			Map<String, Object> mapServiceContractInfo = (Map<String, Object>)mapResponse.get('objServiceContract');
			System.assertEquals(200, res.statusCode);
			System.assertEquals(null, (String)mapAccountInfo.get('SendMethod__c'));
			System.assertEquals(null, (String)mapServiceContractInfo.get('CORE_Payment_Method__c'));
		}
	}

	/*
	* testMethodRestGetAccountDetail_6
	* @created: 2020/06/18 KSVC Nguyen The Phuong
	* @modified: 
	*/
	static testMethod void testMethodRestGetAccountDetail_6() {
		System.runAs(objUserTest) {
			RestRequest req = new RestRequest();
			RestResponse res = new RestResponse();
			req.requestURI = '/services/apexrest/AccountInfoDetail/11223344';
			req.httpMethod = 'GET';
			req.addHeader('Content-Type', 'application/json'); 
			RestContext.request = req;
			RestContext.response = res;
			Test.startTest();
			Core_AccountInfoDetail.getAccountDetail();
			Test.stopTest();

			System.assertEquals(404, res.statusCode);
		}
	}

	/*
	* createServiceContract
	* @param: accId
	* @param: productId
	* @param: paymentMethod
	* @return: なし
	* @created: 2020/06/18 KSVC Nguyen The Phuong
	* @modified:
	*/
	private static void createServiceContract(String accId, String productId, String paymentMethod) {
		ServiceContract objServiceContract = new ServiceContract(
			Name = 'Test'
			, AccountId = accId
			, CORE_Product__c  = productId
			, CORE_Payment_Method__c = paymentMethod
		);
		insert objServiceContract;
	}
	
	/*
	* createProduct2
	* @param: なし
	* @return: Product2
	* @created: 2020/06/18 KSVC Nguyen The Phuong
	* @modified:
	*/
	private static Product2 createProduct2() {
		String recordTypeUnderwriter = [SELECT Id FROM RecordType WHERE Name = 'Underwriter' OR DeveloperName = 'Underwriter' LIMIT 1].Id;
		Account objAccount = createAccount(recordTypeUnderwriter, true, true, true);
		Product2 objProduct2 = new Product2(
			Name = 'Test'
			, CORE_Cancellation_Noticed_Period_Days__c = 10
			, CORE_Product_Code__c = 'Test'
			, CORE_Underwriter__c = objAccount.Id
		);
		insert objProduct2;
		return objProduct2;
	}
	
	/*
	* createOpportunity
	* @param: accId
	* @param: strLeadSource
	* @return: なし
	* @created: 2020/06/18 KSVC Nguyen The Phuong
	* @modified:
	*/
	private static void createOpportunity(String accId, String strLeadSource) {
		Opportunity objOpportunity = new Opportunity(
			AccountId = accId
			, Name = 'Test'
			, StageName = 'KSVC'
			, CloseDate = Date.today().addmonths(1)
			, LeadSource = strLeadSource
		);
		insert objOpportunity;
	}
	
	/*
	* createCampaignMember
	* @param: leadId
	* @return: なし
	* @created: 2020/06/18 KSVC Nguyen The Phuong
	* @modified:
	*/
	private static void createCampaignMember(String leadId) {
		Campaign objCampaign = new Campaign(
			Name = 'Test'
			, CORE_CampaignCode__c = 'Test'
		);
		insert objCampaign;
		CampaignMember objCampaignMember = new CampaignMember(
			LeadId = leadId
			, CampaignId = objCampaign.Id
		);
		insert objCampaignMember;
	}
	
	/*
	* createLead
	* @param: customerNumber
	* @return: なし
	* @created: 2020/06/18 KSVC Nguyen The Phuong
	* @modified:
	*/
	private static Lead createLead(String customerNumber) {
		Lead objLead = new Lead(
			JPN_CustomerNumber__c = customerNumber
			, Lead_Code__c = '11223344'
			, Country = 'Japan'
			, State = 'Kanagawa'
			, City = 'Yokohama Kanazawa Ward'
			, Street = 'Seto'
			, PostalCode = '236-0027'
			, LastName = 'Test'
			, Company = 'KSVC'
			, CORE_dateOfBirth__c = Date.newInstance(1890, 1, 1)
		);
		insert objLead;
		return objLead;
	}

	/*
	* createAccount
	* @param: recordTypeId
	* @param: email
	* @param: mailCheckBox
	* @param: both
	* @return: Account
	* @created: 2020/06/18 KSVC Nguyen The Phuong
	* @modified:
	*/
	private static Account createAccount(String recordTypeId, Boolean email, Boolean mailCheckBox, Boolean both) {
		Account objAccount = new Account(
			Name = 'Test'
			, JPN_nameKana__c = 'テスト'
			, BillingCountry = 'Japan'
			, BillingPostalCode = '9999'
			, BillingState = 'Kanagawa'
			, BillingCity = 'Tokyo'
			, BillingStreet = 'Nagawa'
			, Phone = '1234567890'
			, CORE_email__c = 'test@testorg.com'
			, JPN_Email__c = email
			, JPN_mailCheckBox__c = mailCheckBox
			, JPN_both__c = both
			, Date_of_Birth__c = Date.newInstance(1890, 5, 1)
			, RecordTypeId = recordTypeId
		);
		insert objAccount;
		return objAccount;
	}

	/*
	* createUser
	* @param: なし
	* @return: User
	* @created: 2020/06/18 KSVC Nguyen The Phuong
	* @modified:
	*/
	private static User createUser() {
		// プロフィール
		Profile profile = [SELECT Id FROM Profile WHERE userType = 'standard' AND (Name='System Administrator' OR Name='システム管理者') LIMIT 1];
		// ユニーク名
		String uniqueName = UserInfo.getOrganizationId() +										// 組織Id
						Datetime.now().format('yyyyMMddHHmmssSSS') +							// 今日付/時間
						Integer.valueOf(math.rint(math.random()* (2000000 - 1)));				// ランダム
		// ユーザ
		User user = new User(
				FirstName = uniqueName.subString(29, 34),						// 名
				LastName = uniqueName.subString(30, 35),						// 姓
				Email = uniqueName + '@keizu.co.jp',							// メール
				UserName = uniqueName + '@keizu.co.jp',							// ユーザ名
				EmailEncodingKey = 'UTF-8',										// メールの文字コード
				Alias = '別名',													// 別名
				TimeZoneSidKey = 'Asia/Tokyo',									// タイムゾーン
				LocaleSidKey = 'ja_JP',											// 地域
				LanguageLocaleKey = 'ja',										// 言語
				ProfileId = profile.Id,											// プロフィールId
				PostalCode = '123456789',										// 郵便番号
				Department = 'test',											// 部署
				Phone = '123456789',											// 電話
				Fax = '123456789',												// Fax
				CompanyName  = 'keizu'											// 担当者(IS)のユーザ.会社名
		);
		insert user;
		return user;
	}
}