/*******************************************************************************
 ** Class Name: WebsiteConfirmPurchaseCrossSell
 ** Description: Web service class to initiate Cross Sales process.
 ** Version: 1.0
 ** Created By: Kz
 ** Modified By:
 *******************************************************************************/
@RestResource(urlMapping = '/WebsiteConfirmPurchaseCrossSell/*')
global with sharing class WebsiteConfirmPurchaseCrossSell {

    // Constant definition
    private static final String PRICEBOOK_NAME  = 'Standard Price Book';
    private static final String LOG_PROCESSNAME = 'WebsiteConfirmPurchaseCrossSell';

    // Error cdoe definition : {error code => http status code, error message}
    private static final Map<String, errDef> err = new Map<String, errDef> {
        'SUCCESS'                   => new errDef(200, System.label.confirmPurchaseSuccessLabel),
        'PARAM_NOLEADCODE'          => new errDef(400, 'Lead code required'),
        'PARAM_NOPAYMENTFREQUENCY'  => new errDef(400, 'Payment frequency required'),
        'PARAM_NOPRODUCTCODE'       => new errDef(400, 'Product code required'),
        'PARAM_NOPAYMENTMETHOD'     => new errDef(400, 'Payment method required'),
        'ERROR_NOLEAD'              => new errDef(404, 'Lead not found'),
        'ERROR_NOPRODUCT'           => new errDef(404, 'Product not found'),
        'ERROR_NOPRICEBOOKENTRY'    => new errDef(404, 'Pricebook entry not found'),
        'ERROR_NOCAMPAIGNMEMBER'    => new errDef(404, 'Campaign member not found'),
        'ERROR_NOCAMPAIGNPRODUCT'   => new errDef(404, 'Campaign product not found'),
        'ERROR_NOACCOUNT'           => new errDef(404, 'Account not found'),
        'ERROR_NOCONTACT'           => new errDef(404, 'Contact not found'),
        'ERROR_EXCEPTION'           => new errDef(500, 'Exception occured')
    };

    // http request 
    private static RestRequest req = RestContext.request;
    private static RestResponse res = RestContext.response;
    private static requestBody reqBody = new requestBody();
    private static responseBody resBody = new responseBody();

    @HttpPost
    global static Void createPurchaseCrossSell() {

        Decimal unitPrice;
        String paymentId;

        List<Lead> leads = new List<Lead>();
        List<Product2> products = new List<Product2>();
        List<PricebookEntry> entries = new List<PricebookEntry>();
        List<CampaignMember> members = new List<CampaignMember>();
        List<Core_Campaign_Product__c> campaignProducts = new List<Core_Campaign_Product__c>();
        List<Account> accounts = new List<Account>();
        List<Contact> contacts = new List<Contact>();

        try {
            reqBody = (requestBody)JSON.deserialize(req.requestBody.toString().replace('JPN_Payment_Method__c', 'paymentMethod'), requestBody.class);
            paymentId = 'Dummy Data';

            // Check parameters - Lead code
            if (String.isBlank(reqBody.leadCode)) {
                createResponse('PARAM_NOLEADCODE');
                return;
            }

            // Check parameters - Payment frequency
            if (String.isBlank(reqBody.paymentfrequency)) {
                createResponse('PARAM_NOPAYMENTFREQUENCY');
                return;
            }

            // Check parameters - Product code
            if (reqBody.prodList == null || String.isBlank(reqBody.prodList[0].productCode)) {
                createResponse('PARAM_NOPRODUCTCODE');
                return;
            }

            // Check parameters - Payment method
            if (String.isBlank(reqBody.paymentMethod)) {
                createResponse('PARAM_NOPAYMENTMETHOD');
                return;
            }

            // Get the lead object based on lead code to get the lead id
            leads = [
                SELECT Id, JPN_CustomerNumber__c, Email, Phone, JPN_both__c, JPN_emailCheckbox__c
                FROM Lead
                WHERE Lead_Code__c = :reqBody.leadCode AND IsConverted = False
                ORDER BY LastModifiedDate DESC
                LIMIT 1];
            if (leads.size() == 0) {
                createResponse('ERROR_NOLEAD');
                return;
            }

            // Querying the product based on product code in request.
            products = [
                SELECT Id, CORE_Product_Code__c 
                FROM Product2 
                WHERE CORE_Product_Code__c = :reqBody.prodList[0].productCode
                ORDER BY LastModifiedDate DESC
                LIMIT 1];
            if (products.size() == 0) {
                createResponse('ERROR_NOPRODUCT');
                return;
            }

            // Get the Standard pricebook related to product.
            entries = [
                SELECT Id
                FROM PricebookEntry 
                WHERE Pricebook2.Name = :PRICEBOOK_NAME AND Product2Id =: products[0].id
                ORDER BY LastModifiedDate DESC
                LIMIT 1];
            if (entries.size() == 0) {
                createResponse('ERROR_NOPRICEBOOKENTRY');
                return;
            }
    
            // Querying the campaign member object to get the campaignid, status based on lead id.
            members = [
                SELECT Id, CampaignId, Status
                FROM CampaignMember
                WHERE LeadId = :leads[0].Id
                ORDER BY LastModifiedDate DESC
                LIMIT 1];
            if (members.size() == 0) {
                createResponse('ERROR_NOCAMPAIGNMEMBER');
                return;
            }

            // Get the Campaign Product related to product and campaign.
            campaignProducts = [
                SELECT Id, JPN_AnnualPrice__c, JPN_Monthly_Price__c
                FROM Core_Campaign_Product__c
                WHERE CORE_campaignId__c = :members[0].CampaignId AND Product_Code__c = :reqBody.prodList[0].productCode
                ORDER BY LastModifiedDate DESC
                LIMIT 1];
            if (campaignProducts.size() == 0) {
                createResponse('ERROR_NOCAMPAIGNPRODUCT');
                return;
            }

            // Determine purchase price based on payment method in request.
            if (reqBody.paymentfrequency == 'Annual') {
                unitPrice = campaignProducts[0].JPN_AnnualPrice__c;
            }
            else if (reqBody.paymentfrequency == 'Monthly') {
                unitPrice = campaignProducts[0].JPN_Monthly_Price__c;
            }

            // Get the account based on customerid in lead.
            accounts = [
                SELECT Id, CORE_email__c, Phone, JPN_both__c, JPN_Email__c
                FROM Account
                WHERE JPN_AccountNumber__c = :leads[0].JPN_CustomerNumber__c
                ORDER BY LastModifiedDate DESC
                LIMIT 1];
            if (accounts.size() == 0) {
                createResponse('ERROR_NOACCOUNT');
                return;
            }

            // Get the contact related to account.
            contacts = [
                SELECT Id, Email, Phone
                FROM Contact
                WHERE AccountId = :accounts[0].Id
                ORDER BY LastModifiedDate DESC
                LIMIT 1];
            if (contacts.size() == 0) {
                createResponse('ERROR_NOCONTACT');
                return;
            }

            // Update the status of campaign members to "Responsed".
            members[0].status = 'レスポンスあり';
            try {
                update members[0];
            }
            catch (Exception ex) {
                System.debug(err.get('ERROR_EXCEPTION').errMessage + ': ' + ex.getMessage());
            }

            // Update account record.
            accounts[0].CORE_email__c = reqBody.emailaddress;
            accounts[0].Phone = reqBody.phonenumber;
            accounts[0].JPN_mailCheckBox__c = false;
            if (reqBody.mailingpreference == 'Both') {
                accounts[0].JPN_both__c = true;
                accounts[0].JPN_Email__c = false;
            }
            else if (reqBody.mailingpreference == 'Email') {
                accounts[0].JPN_both__c = false;
                accounts[0].JPN_Email__c = true;
            }
            try {
                update accounts[0];
            }
            catch (Exception ex) {
                System.debug(err.get('ERROR_EXCEPTION').errMessage + ': ' + ex.getMessage());
            }

            // Update contact record.
            contacts[0].Email = reqBody.emailaddress;
            contacts[0].Phone = reqBody.phonenumber;
            try {
                update contacts[0];
            }
            catch (Exception ex) {
                System.debug(err.get('ERROR_EXCEPTION').errMessage + ': ' + ex.getMessage());
            }

            // Update lead record.
            leads[0].Email = reqBody.emailaddress;
            leads[0].Phone = reqBody.phonenumber;
            leads[0].JPN_mailCheckBox__c = false;
            if (reqBody.mailingpreference == 'Both') {
                leads[0].JPN_both__c = true;
                leads[0].JPN_emailCheckbox__c = false;
            }
            else if (reqBody.mailingpreference == 'Email') {
                leads[0].JPN_both__c = false;
                leads[0].JPN_emailCheckbox__c = true;
            }
            try {
                update leads[0];
            }
            catch (Exception ex) {
                System.debug(err.get('ERROR_EXCEPTION').errMessage + ': ' + ex.getMessage());
            }

            // Convert lead into account, contact, and opportunity
            AutoConvertLeads.LeadConvertToExistingAccount(
                new List<Id>{leads[0].Id},
                accounts[0].Id,
                contacts[0].Id);

            // Handling the converted lead to update opportunity to "Closed-Won".
            WebsiteConfirmPurchaseHandleConvLead.handleConvertedLeadCrossSell(
                leads[0].Id,
                entries[0].Id,
                unitPrice, 
                reqBody.paymentfrequency, 
                reqBody.customerid,
                paymentId,
                products[0].Id,
                reqBody.paymentMethod);

            // Create a success response.
            createResponse('SUCCESS');
            return;
        }
        catch (Exception ex) {
            createResponse('ERROR_EXCEPTION', ex);
            return;
        }
    }

	private static void createResponse(String code) {
		createResponse(code, null);
	}

    private static void createResponse(String code, Exception ex) {
		resBody.responseMessage = err.get(code).errMessage;
		if (ex != null) {
			resBody.responseMessage += ' - ' + ex.getMessage();
		}
        res.statusCode = err.get(code).statusCode;
        res.responseBody = Blob.valueOf(JSON.Serialize(resBody));  
        CORE_IntegrationLogHandler.webserviceResponseCatcher(req, JSON.Serialize(resBody), LOG_PROCESSNAME); 
    }

    global class requestBody {
        global String leadCode {
            get;
            set;
        }
        global String emailaddress {
            get;
            set;
        }
        global String phonenumber {
            get;
            set;
        }
        global String paymentfrequency {
            get;
            set;
        }
        global String customerid {
            get;
            set;
        }
        global String mailingpreference  {
            get;
            set;
        }
        global String newPayment {
            get;
            set;
        }
        global List<productInfo> prodList {
            get;
            set;
        }
        global String paymentMethod {
            get;
            set;
        }
    }

    global class productInfo {
        global String productCode {
            get;
            set;
        }
    }

    global class responseBody {
        global string responseMessage {
            get;
            set;
        }
    }

    private class errDef {
        Integer statusCode;
        String errMessage;
        errDef (Integer a, String b) {
            statusCode = a;
            errMessage = b;
        }
    }
}