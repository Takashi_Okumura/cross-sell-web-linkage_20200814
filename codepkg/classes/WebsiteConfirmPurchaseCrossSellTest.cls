/**
 * クラス名: WebsiteConfirmPurchaseCrossSellTest
 * クラス概要: 
 * @created: 2020/06/30 Hoang Long
 */
@isTest
public class WebsiteConfirmPurchaseCrossSellTest {
    private static User objUser;
    private static List<Lead> lstLead;
    private static List<CampaignMember> lstCampaign;
    private static Campaign objCampaign;
    private static Campaign objCampaign1;
    private static Contact objContact;
    private static Schema.Location objLocation;
    static{

        objUser = createUser();
        System.runAs(objUser){
            List<Product2> prodList = new List<Product2>();
            List<PricebookEntry> pbeList = new List<PricebookEntry>();
            List<Pricing__c> pricingList = new List<Pricing__c>();
            List<CORE_productAffinityPartner__c> coreProdAffList = new List<CORE_productAffinityPartner__c>();
            List<Core_Campaign_Product__c> cmpProdList = new List<Core_Campaign_Product__c>();

            RecordType objRecord = getRecordType();
            RecordType objRecord2 = getRecordType2();
            RecordType objRecordTypCustom = getRecordTypeIsCustom();

            Account acc = createAccount(objRecord.Id, 'Test partner', '123');
            Account accCustom = createAccount(objRecordTypCustom.Id, 'AccountCustom', '12555');
            Account accJPNAcc = [SELECT Id,
                                        CORE_email__c,
                                        Phone,
                                        JPN_both__c,
                                        JPN_Email__c,
                                        JPN_AccountNumber__c
                                FROM Account 
                                WHERE ID = :accCustom.Id];
            objContact = creatContact(accCustom.Id);
            objLocation = creatLocation(accCustom.Id);

            Account underwriteracc = createAccount(objRecord2.Id, 'Test underwriter','1111');

            objCampaign = createCampaign(acc.Id, 'Testing Campaign', '944135');
            objCampaign1 = createCampaign(acc.id, 'Testing Campaign', '9441351');

            Product2 product = new product2(Name = 'Test Product',
                                          Isactive = FALSE,
                                          CORE_Product_Code__c = 'Product00025', 
                                          CORE_Underwriter__c = underwriteracc.id,
                                          CORE_benefitAmountLimits__c=1000.00,
                                          JPN_Claim_Waiting_Period__c = 2,
                                          JPN_No_Of_Claim_Calls__c = 2,
                                          JPN_Product_Excess_Payment__c = 900.00,
                                          CORE_Cancellation_Noticed_Period_Days__c = 10);
            prodList.add(product);
            Product2 product1 = new product2(Name = 'Test Product1',
                                           Isactive = FALSE,
                                           CORE_Product_Code__c = 'Product00026',
                                           CORE_Underwriter__c = underwriteracc.id,
                                           CORE_benefitAmountLimits__c = 1000.00,
                                           JPN_Claim_Waiting_Period__c = 2,
                                           JPN_No_Of_Claim_Calls__c = 2,
                                           JPN_Product_Excess_Payment__c = 900.00,
                                           CORE_Cancellation_Noticed_Period_Days__c = 10);
            prodList.add(product1);
            insert prodList;

            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );
		    standardPricebook.Name = 'Standard Price Book';
            Update standardPricebook;
        
            standardPricebook = [SELECT Id,
                                        IsStandard 
                                 FROM Pricebook2 
                                 WHERE Id = :standardPricebook.Id];
        
            PricebookEntry pbe = new PricebookEntry(
                Pricebook2Id = standardPricebook.Id,
                Product2Id = product.Id,
                UnitPrice = 1000,
                IsActive = true
            );
            pbeList.add(pbe);
            PricebookEntry pbe1 = new PricebookEntry();
            pbe1.Product2Id = product1.id;
            pbe1.Pricebook2id = standardPricebook.Id;
            pbe1.IsActive = true;
            pbe1.UnitPrice = 1000;
            pbeList.add(pbe1);
            insert pbeList;

            CORE_productAffinityPartner__c productaffinitypartner = new CORE_productAffinityPartner__c(CORE_affinityPartner__c = acc.id,
                                                                                                   CORE_StandardPrice__c = 1000,
                                                                                                   CORE_product__c = product.id);
            coreProdAffList.add(productaffinitypartner);
            CORE_productAffinityPartner__c productaffinitypartner1 = new CORE_productAffinityPartner__c(CORE_affinityPartner__c = acc.id,
                                                                                                    CORE_StandardPrice__c = 1000,
                                                                                                    CORE_product__c = product1.id);
            coreProdAffList.add(productaffinitypartner1);
            CORE_productAffinityPartner__c productaffinitypartner2 = new CORE_productAffinityPartner__c(CORE_affinityPartner__c = acc.id,
                                                                                                    CORE_StandardPrice__c = 1000,
                                                                                                    CORE_product__c = product1.id);
            coreProdAffList.add(productaffinitypartner2);
            CORE_productAffinityPartner__c productaffinitypartner3 = new CORE_productAffinityPartner__c(CORE_affinityPartner__c = acc.id,
                                                                                                   CORE_StandardPrice__c = 1000,
                                                                                                   CORE_product__c = product.id);
            coreProdAffList.add(productaffinitypartner3);
            insert coreProdAffList;

            Pricing__c pricing = new Pricing__c(	Product_Affinity_Partner__c = productaffinitypartner.id, Final_Price_Annual_contract__c = 2000);
            pricingList.add(pricing);
            Pricing__c pricing1 = new Pricing__c(Product_Affinity_Partner__c = productaffinitypartner1.id, Final_Price_Annual_contract__c = 2000);
            pricingList.add(pricing1);
            insert pricingList;

            Core_Campaign_Product__c campaignProduct = new Core_Campaign_Product__c(CORE_campaignId__c = objCampaign.Id,
                                                                                CORE_Product__c = productaffinitypartner.Id,
                                                                                CORE_Discount__c = 0,
                                                                                CORE_PrimaryProduct__c = TRUE);
            Core_Campaign_Product__c campaignProduct1 = new Core_Campaign_Product__c(CORE_campaignId__c = objCampaign1.Id,
                                                                                 CORE_Product__c = productaffinitypartner1.Id,
                                                                                 CORE_Discount__c = 0,
                                                                                 CORE_PrimaryProduct__c = TRUE);
            Core_Campaign_Product__c campaignProduc2 = new Core_Campaign_Product__c(CORE_campaignId__c = objCampaign1.Id,
                                                                                CORE_Product__c = productaffinitypartner.Id,
                                                                                CORE_Discount__c = 0,
                                                                                CORE_PrimaryProduct__c = TRUE);
            Core_Campaign_Product__c campaignProduct3 = new Core_Campaign_Product__c(CORE_campaignId__c = objCampaign.Id,
                                                                                CORE_Product__c = productaffinitypartner1.Id,
                                                                                CORE_Discount__c = 0,
                                                                                CORE_PrimaryProduct__c = TRUE);
            cmpProdList.add(campaignProduct);
            cmpProdList.add(campaignProduct1);
            cmpProdList.add(campaignProduc2);
            cmpProdList.add(campaignProduct3);
            insert cmpProdList;

            lstLead = createLead(accJPNAcc.JPN_AccountNumber__c);
            lstCampaign = createCampaign(lstLead, objCampaign.Id);
            }
    }
    /**
     * testmethodCaseCorrect
     * @param: なし
     * @return: なし
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
    static testmethod void testmethodCaseCorrect(){	
        System.runAs(objUser){
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/WebsiteConfirmPurchaseCrossSell';
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); 
            String jsonString = '{"leadCode":"8698944"';
            jsonString += ',"emailaddress":"testApi@test.com"';
            jsonString += ',"phonenumber":"8698944135"';
            jsonString += ',"mailingpreference":"Email"';
            jsonString += ',"paymentfrequency":"Annual"';
            jsonString += ',"customerid":"9856"';
            jsonString += ',"newPayment":"True"';
            jsonString += ',"prodList":[{"productCode":"Product00025"}]';
            jsonString += ',"JPN_Payment_Method__c":"Credit Card"}';

            req.requestBody = Blob.valueof(jsonString);
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            WebsiteConfirmPurchaseCrossSell.createPurchaseCrossSell();
            Test.stopTest();
         	List<CampaignMember> lstCampaignUpdate = [SELECT Id,
                                                      		 Status 
                                                      FROM CampaignMember ORDER BY LastModifiedDate ASC];
            system.assertEquals('レスポンスあり', lstCampaignUpdate[0].status);
        }
    }
     /**
     * testmethodLeadEmailEx
     * @param: なし
     * @return: なし
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
    static testmethod void testmethodLeadEmailEx(){	
        System.runAs(objUser){
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/WebsiteConfirmPurchaseCrossSell';
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); 
            String jsonString = '{"leadCode":"8698944"';
            jsonString += ',"emailaddress":"dcmJapan"';
            jsonString += ',"phonenumber":"8698944135"';
            jsonString += ',"mailingpreference":"Email"';
            jsonString += ',"paymentfrequency":"Annual"';
            jsonString += ',"customerid":"9856"';
            jsonString += ',"newPayment":"True"';
            jsonString += ',"prodList":[{"productCode":"Product00025"}]';
            jsonString += ',"JPN_Payment_Method__c":"Credit Card"}';
            req.requestBody = Blob.valueof(jsonString);
            RestContext.request = req;
            RestContext.response = res;
            try{
                Test.startTest();
                WebsiteConfirmPurchaseCrossSell.createPurchaseCrossSell();
                Test.stopTest();
            }catch(Exception ex){
                system.assertEquals('Exception occured', ex.getMessage());
            }
        }
    }
    /**
     * testmethodLeadCodeNull
     * @param: なし
     * @return: なし
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
    static testmethod void testmethodLeadCodeNull(){
        System.runAs(objUser){
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/WebsiteConfirmPurchaseCrossSell';
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json');
            String jsonString = '{"leadCode":""';
            jsonString += ',"emailaddress":"testAPI@test.com"';
            jsonString += ',"phonenumber":"8698944135"';
            jsonString += ',"mailingpreference":"Email"';
            jsonString += ',"paymentfrequency":"Annual"';
            jsonString += ',"customerid":"9856"';
            jsonString += ',"newPayment":"True"';
            jsonString += ',"prodList":[{"productCode":"Product00025"}]';
            jsonString += ',"JPN_Payment_Method__c":"Credit Card"}';
            req.requestBody = Blob.valueof(jsonString);
            RestContext.request = req;
            RestContext.response = res;
            WebsiteConfirmPurchaseCrossSell.responseBody resp =new  WebsiteConfirmPurchaseCrossSell.responseBody();
            Test.startTest();
            WebsiteConfirmPurchaseCrossSell.createPurchaseCrossSell();
            Test.stopTest();

            System.assertNotEquals(null, resp);
        }
    }
    /**
     * testmethodPaymentNull
     * @param: なし
     * @return: なし
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
     static testmethod void testmethodPaymentNull(){
        System.runAs(objUser){
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/WebsiteConfirmPurchaseCrossSell';
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); 
            String jsonString = '{"leadCode":"8698944"';
            jsonString += ',"emailaddress":"testApi@test.com"';
            jsonString += ',"phonenumber":"8698944135"';
            jsonString += ',"mailingpreference":"Email"';
            jsonString += ',"paymentfrequency":""';
            jsonString += ',"customerid":"9856"';
            jsonString += ',"newPayment":"True"';
            jsonString += ',"prodList":[{"productCode":"Product00025"}]';
            jsonString += ',"JPN_Payment_Method__c":"Credit Card"}';
            req.requestBody = Blob.valueof(jsonString);
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            WebsiteConfirmPurchaseCrossSell.requestBody  rqBody = new WebsiteConfirmPurchaseCrossSell.requestBody();
           
            
            WebsiteConfirmPurchaseCrossSell.createPurchaseCrossSell();
            Test.stopTest();
            system.assertEquals(null,  rqBody.newPayment);
            system.assertEquals(null,  rqBody.customerid);
        }
    }
    /**
     * testmethodProlistNull
     * @param: なし
     * @return: なし
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
     static testmethod void testmethodProlistNull(){
        System.runAs(objUser){
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/WebsiteConfirmPurchaseCrossSell';
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); 
            String jsonString = '{"leadCode":"8698944"';
            jsonString += ',"emailaddress":"testApi@test.com"';
            jsonString += ',"phonenumber":"8698944135"';
            jsonString += ',"mailingpreference":"Email"';
            jsonString += ',"paymentfrequency":"Annual"';
            jsonString += ',"customerid":"9856"';
            jsonString += ',"newPayment":"True"';
            jsonString += ',"prodList":[{"productCode":""}]';
            jsonString += ',"JPN_Payment_Method__c":"Credit Card"}';
            req.requestBody = Blob.valueof(jsonString);
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            WebsiteConfirmPurchaseCrossSell.createPurchaseCrossSell();
            Test.stopTest();
        }
    }
    /**
     * testmethodlstLeadEmp
     * @param: なし
     * @return: なし
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
    static testmethod void testmethodlstLeadEmp(){	
        System.runAs(objUser){
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/WebsiteConfirmPurchaseCrossSell';
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); 
            String jsonString = '{"leadCode":"18698944"';
            jsonString += ',"emailaddress":"testApi@test.com"';
            jsonString += ',"phonenumber":"8698944135"';
            jsonString += ',"mailingpreference":"Email"';
            jsonString += ',"paymentfrequency":"Annual"';
            jsonString += ',"customerid":"9856"';
            jsonString += ',"newPayment":"True"';
            jsonString += ',"prodList":[{"productCode":"Product00025"}]';
            jsonString += ',"JPN_Payment_Method__c":"Credit Card"}';
            req.requestBody = Blob.valueof(jsonString);
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            WebsiteConfirmPurchaseCrossSell.createPurchaseCrossSell();
            Test.stopTest();
            
        }
    }
    /**
     * testmethodlstProdEmp
     * @param: なし
     * @return: なし
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
     static testmethod void testmethodlstProdEmp(){
        System.runAs(objUser){
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/WebsiteConfirmPurchaseCrossSell';
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json');
            String jsonString = '{"leadCode":"8698944"';
            jsonString += ',"emailaddress":"testApi@test.com"';
            jsonString += ',"phonenumber":"8698944135"';
            jsonString += ',"mailingpreference":"Email"';
            jsonString += ',"paymentfrequency":"Annual"';
            jsonString += ',"customerid":"9856"';
            jsonString += ',"newPayment":"True"';
            jsonString += ',"prodList":[{"productCode":"Prod4000"}]';
            jsonString += ',"JPN_Payment_Method__c":"Credit Card"}';
            req.requestBody = Blob.valueof(jsonString);
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            WebsiteConfirmPurchaseCrossSell.createPurchaseCrossSell();
            Test.stopTest();
        }
    }
    /**
     * testmethodlstPBookEmp
     * @param: なし
     * @return: なし
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
    static testmethod void testmethodlstPBookEmp(){	
        System.runAs(objUser){
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/WebsiteConfirmPurchaseCrossSell';
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json');
            String jsonString = '{"leadCode":"8698944"';
            jsonString += ',"emailaddress":"testApi@test.com"';
            jsonString += ',"phonenumber":"8698944135"';
            jsonString += ',"mailingpreference":"Email"';
            jsonString += ',"paymentfrequency":"Annual"';
            jsonString += ',"customerid":"9856"';
            jsonString += ',"newPayment":"True"';
            jsonString += ',"prodList":[{"productCode":"Product00025"}]';
            jsonString += ',"JPN_Payment_Method__c":"Credit Card"}';
            req.requestBody = Blob.valueof(jsonString);
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            delete [Select Id from PricebookEntry];
            WebsiteConfirmPurchaseCrossSell.createPurchaseCrossSell();
            Test.stopTest();
        }
    }
     /**
     * testmethodlstCampMemberEmp
     * @param: なし
     * @return: なし
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
     static testmethod void testmethodlstCampMemberEmp(){
        System.runAs(objUser){
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/WebsiteConfirmPurchaseCrossSell';
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json');
            String jsonString = '{"leadCode":"8698944"';
            jsonString += ',"emailaddress":"testApi@test.com"';
            jsonString += ',"phonenumber":"8698944135"';
            jsonString += ',"mailingpreference":"Email"';
            jsonString += ',"paymentfrequency":"Annual"';
            jsonString += ',"customerid":"9856"';
            jsonString += ',"newPayment":"True"';
            jsonString += ',"prodList":[{"productCode":"Product00025"}]';
            jsonString += ',"JPN_Payment_Method__c":"Credit Card"}';
            req.requestBody = Blob.valueof(jsonString);
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            delete [Select Id FROM CampaignMember];
            WebsiteConfirmPurchaseCrossSell.createPurchaseCrossSell();
            Test.stopTest();
            
        }
    }
     /**
     * testmethodlstCoreCmpEmp
     * @param: なし
     * @return: なし
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
     static testmethod void testmethodlstCoreCmpEmp(){
        System.runAs(objUser){
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/WebsiteConfirmPurchaseCrossSell';
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); 
            String jsonString = '{"leadCode":"8698944"';
            jsonString += ',"emailaddress":"testApi@test.com"';
            jsonString += ',"phonenumber":"8698944135"';
            jsonString += ',"mailingpreference":"Email"';
            jsonString += ',"paymentfrequency":"Annual"';
            jsonString += ',"customerid":"9856"';
            jsonString += ',"newPayment":"True"';
            jsonString += ',"prodList":[{"productCode":"Product00025"}]';
            jsonString += ',"JPN_Payment_Method__c":"Credit Card"}';
            req.requestBody = Blob.valueof(jsonString);
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            delete [SELECT Id FROM Core_Campaign_Product__c];
            WebsiteConfirmPurchaseCrossSell.createPurchaseCrossSell();
            Test.stopTest();
        }
    }
    /**
     * testmethodPayEqualsMonthly
     * @param: なし
     * @return: なし
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
     static testmethod void testmethodPayEqualsMonthly(){	
        System.runAs(objUser){
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/WebsiteConfirmPurchaseCrossSell';
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); 
            String jsonString = '{"leadCode":"8698944"';
            jsonString += ',"emailaddress":"testApi@test.com"';
            jsonString += ',"phonenumber":"8698944135"';
            jsonString += ',"mailingpreference":"Email"';
            jsonString += ',"paymentfrequency":"Monthly"';
            jsonString += ',"customerid":"9856"';
            jsonString += ',"newPayment":"True"';
            jsonString += ',"prodList":[{"productCode":"Product00025"}]';
            jsonString += ',"JPN_Payment_Method__c":"Credit Card"}';
            req.requestBody = Blob.valueof(jsonString);
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            WebsiteConfirmPurchaseCrossSell.createPurchaseCrossSell();
            Test.stopTest();
        }
    }
    /**
     * testmethodAccEmp
     * @param: なし
     * @return: なし
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
     static testmethod void testmethodAccEmp(){
        System.runAs(objUser){
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/WebsiteConfirmPurchaseCrossSell';
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json');
            String jsonString = '{"leadCode":"8698944"';
            jsonString += ',"emailaddress":"testApi@test.com"';
            jsonString += ',"phonenumber":"8698944135"';
            jsonString += ',"mailingpreference":"Email"';
            jsonString += ',"paymentfrequency":"Annual"';
            jsonString += ',"customerid":"9856"';
            jsonString += ',"newPayment":"True"';
            jsonString += ',"prodList":[{"productCode":"Product00025"}]';
            jsonString += ',"JPN_Payment_Method__c":"Credit Card"}';
            req.requestBody = Blob.valueof(jsonString);
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
           	lstLead[0].JPN_CustomerNumber__c = '';
            update lstLead;
            WebsiteConfirmPurchaseCrossSell.createPurchaseCrossSell();
            Test.stopTest();
        }
    }
    /**
     * testmethodContactEmp
     * @param: なし
     * @return: なし
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
     static testmethod void testmethodContactEmp(){
        System.runAs(objUser){
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/WebsiteConfirmPurchaseCrossSell';
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json');
            String jsonString = '{"leadCode":"8698944"';
            jsonString += ',"emailaddress":"testApi@test.com"';
            jsonString += ',"phonenumber":"8698944135"';
            jsonString += ',"mailingpreference":"Email"';
            jsonString += ',"paymentfrequency":"Annual"';
            jsonString += ',"customerid":"9856"';
            jsonString += ',"newPayment":"True"';
            jsonString += ',"prodList":[{"productCode":"Product00025"}]';
            jsonString += ',"JPN_Payment_Method__c":"Credit Card"}';
            req.requestBody = Blob.valueof(jsonString);
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            objContact.Accountid = null;
            update objContact;
            WebsiteConfirmPurchaseCrossSell.createPurchaseCrossSell();
            Test.stopTest();
        }
    }
    /**
     * testmethodMallingBoth
     * @param: なし
     * @return: なし
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
     static testmethod void testmethodMallingBoth(){
        System.runAs(objUser){
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/WebsiteConfirmPurchaseCrossSell';
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json');
            String jsonString = '{"leadCode":"8698944"';
            jsonString += ',"emailaddress":"testApi@test.com"';
            jsonString += ',"phonenumber":"8698944135"';
            jsonString += ',"mailingpreference":"Both"';
            jsonString += ',"paymentfrequency":"Annual"';
            jsonString += ',"customerid":"9856"';
            jsonString += ',"newPayment":"True"';
            jsonString += ',"prodList":[{"productCode":"Product00025"}]';
            jsonString += ',"JPN_Payment_Method__c":"Credit Card"}';
            req.requestBody = Blob.valueof(jsonString);
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            WebsiteConfirmPurchaseCrossSell.createPurchaseCrossSell();
            Test.stopTest();
        }
    }
    /**
     * testmethodMallingEmail
     * @param: なし
     * @return: なし
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
     static testmethod void testmethodMallingEmail(){
        System.runAs(objUser){
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/WebsiteConfirmPurchaseCrossSell';
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json');
            String jsonString = '{"leadCode":"8698944"';
            jsonString += ',"emailaddress":"testApi"';
            jsonString += ',"phonenumber":"8698944135"';
            jsonString += ',"mailingpreference":"Email"';
            jsonString += ',"paymentfrequency":"Annual"';
            jsonString += ',"customerid":"9856"';
            jsonString += ',"newPayment":"True"';
            jsonString += ',"prodList":[{"productCode":"Product00025"}]';
            jsonString += ',"JPN_Payment_Method__c":"Credit Card"}';
            req.requestBody = Blob.valueof(jsonString);
            RestContext.request = req;
            RestContext.response = res;
            Test.startTest();
            WebsiteConfirmPurchaseCrossSell.createPurchaseCrossSell();
            Test.stopTest();
        }
    }
    /**
     * createLead
     * @param: strNumber
     * @return: leadList
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
    private static List<Lead> createLead(String strNumber){
        List<Lead> leadList = new List<Lead>();
            Lead objLead = new Lead();
            objLead.FirstName = 'TestLeadIntegration';
            objLead.LastName = 'Lead 1';
            objLead.Company = 'Test Company';
            objLead.JPN_fld_identityNumber__c = '1234232';
        	objLead.JPN_CustomerNumber__c = strNumber;
            objLead.JPN_gender__c = 'Male';
            objLead.CORE_dateOfBirth__c = Date.newInstance(1990, 7, 18);
            objLead.Status = 'Qualified';
            objLead.Email = 'Test@email.com';
            objLead.Country = 'Japan';
            objLead.State = 'Kanagawa';
            objLead.City = 'Yokohama Kanazawa Ward';
            objLead.Street = 'Seto';
            objLead.PostalCode = '236-0027';
            objLead.JPN_emailCheckbox__c = true;
            objLead.JPN_both__c = false;
            objLead.JPN_mailCheckbox__c = false;
            objLead.JPN_AddressCode__c = '45458787';
            objLead.Lead_Code__c = '8698944';
            leadList.add(objLead);
        insert leadList;  
        return leadList;
    }
    /**
     * createCampaign
     * @param: lstLead
     * @param: strIdCamp
     * @return: lstCampaign
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
    private static List<CampaignMember> createCampaign(List<Lead> lstLead, Id strIdCamp){
        List<CampaignMember> lstCampaign = new List<CampaignMember>();
        for(Lead item :lstLead){
            CampaignMember cm = new CampaignMember();
            cm.CampaignId = strIdCamp;
            cm.LeadId = item.id;
            cm.Status = '送信';
            lstCampaign.add(cm);
        }
        insert lstCampaign;
        return lstCampaign;
    }
    /**
     * createCampaign
     * @param: strAccId
     * @param: strName
     * @param: strCoreCmpCode
     * @return: camp
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
    private static Campaign createCampaign(string strAccId, string strName, string strCoreCmpCode){
         Campaign camp = new Campaign(Name = strName,
                                      CORE_Partner__c = strAccId, 
                                      CORE_CampaignCode__c = strCoreCmpCode);
        insert camp;
        return camp;
    }
    /**
     * createAccount
     * @param: strIdRT
     * @param: strName
     * @return: acc
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
    private static Account createAccount(string strIdRT, string strName, String strIdentityNumber){
        Account acc = new Account(Name = strName,
                                CORE_identityNumber__c = strIdentityNumber, 
                                Preferred_Channel__c = 'Mail',
                                BillingStreet = 'CMT8',
                                BillingCity = 'hcm',
                                BillingState = 'dictric3',
                                BillingPostalCode = '70000',
                                BillingCountry = 'VN',
                                Date_of_Birth__c = Date.today().addYears(-20),
                                CORE_email__c = 'test@test.com',
                                Phone = '0312345678',
                                JPN_both__c = false,
                                RecordTypeId = strIdRT);
        insert acc;
        return acc;
    }
    /**
     * creatContact
     * @param: strIdAcc
     * @return: cont
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
    private static Contact creatContact(string strIdAcc){
        Contact cont = new Contact();
		cont.FirstName = 'Test';
		cont.LastName = 'Test';
		cont.Accountid = strIdAcc;
		insert cont;
        return cont;
    }
    /**
     * creatLocation
     * @param: strIdAcc
     * @return: location
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
    private static Schema.Location creatLocation(string strIdAcc){
        Schema.Location loc = new Schema.Location();
        loc.Name = 'Location';
		loc.CORE_Account__c = strIdAcc;
		insert loc;
        return loc;
    }
    /**
     * getRecordType
     * @param: なし
     * @return: objRecord
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
    private static RecordType getRecordType(){
        RecordType objRecord= [SELECT DeveloperName,
                               		  Id,
                               		  Name
                               FROM RecordType 
                               WHERE DeveloperName = 'Affinity_Partner'
                               LIMIT 1];
        return objRecord;
    }
    /**
     * getRecordType2
     * @param: なし
     * @return: objRecord
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
    private static RecordType getRecordType2(){
        RecordType objRecord= [SELECT DeveloperName,
                                      Id,
                                      Name
                               FROM RecordType 
                               WHERE DeveloperName = 'Underwriter' 
                               LIMIT 1];
        return objRecord;
    }
    /**
     * getRecordTypeIsCustom
     * @param: なし
     * @return: objRecord
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
    private static RecordType getRecordTypeIsCustom(){
        RecordType objRecord= [SELECT DeveloperName,
                                      Id,
                                      Name
                               FROM RecordType 
                               WHERE DeveloperName = 'Customer' 
                               LIMIT 1];
        return objRecord;
    }
    /**
     * createUser
     * @param: なし
     * @return: user
     * @created: 2020/06/30 Hoang Long
     * @modified: 
     */
	private static User createUser() {
		// プロファイル
		Profile profile =  [SELECT Id FROM Profile WHERE Name='HS API User'];
		String uniqueName = UserInfo.getOrganizationId() +              // 組織Id
		Datetime.now().format('yyyyMMddHHmmssSSS') +                // 今日付/時間
		Integer.valueOf(math.rint(math.random()* (2000000 - 1)));   // ランダム
		User user = new User(
			FirstName           = uniqueName.subString(29, 34),         // 名
			LastName            = uniqueName.subString(30, 35),         // 姓
			Email               = uniqueName + '@keizu.co.jp',          // メール
			UserName            = uniqueName + '@keizu.co.jp',          // ユーザ名
			EmailEncodingKey    = 'UTF-8',                              // メールの文字コード
			Alias               = uniqueName.subString(29, 34),         // 別名
			TimeZoneSidKey      = 'Asia/Tokyo',                         // タイムゾーン
			LocaleSidKey        = 'ja_JP',                              // 地域
			LanguageLocaleKey   = 'ja',                                 // 言語
			ProfileId           = profile.Id,                           // プロフィールId
			PostalCode          = '123456789',                          // 郵便番号
			Department          = 'test',                               // 部署
			Phone               = '123456789',                          // 電話
			Fax                 = '123456789'                           // Fax
		);
		insert user;
		return user;
	}
}