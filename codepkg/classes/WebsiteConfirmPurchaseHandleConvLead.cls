public class WebsiteConfirmPurchaseHandleConvLead {
	public static void handleConvertedLead(string leadidinSalesforce, string prbEId, Decimal payFrequencyToCheck, string payFrequency, string tokenIdInRequest, string paymentIdInRequest,string productIdToAdd) {
		List < Lead > lObjConverted = new List < Lead > ();
		List < Opportunity > oppObjConverted = new List < Opportunity > ();
		List < Schema.Location > locList = new List < Schema.Location > ();
		List < Opportunity > convertOppUpdate = new List < Opportunity > ();
		List < Opportunity > closeWonList = new List < Opportunity > ();
		lObjConverted = [Select id, name, ConvertedContactId, ConvertedopportunityId, ConvertedAccountId from Lead where isconverted = true and id =: leadidinSalesforce];
		If(lObjConverted.size() > 0) {
            system.debug('Account id'+lObjConverted[0].ConvertedAccountId);
			oppObjConverted = [Select id, Name from Opportunity where id =: lObjConverted[0].ConvertedopportunityId];
			locList = [Select Id,CORE_Account__c FROM location where CORE_Account__c =: lObjConverted[0].ConvertedAccountId Limit 1];
            system.debug('locList'+locList);
            
			if (oppObjConverted.size() > 0) {
				OpportunityLineItem OLI = new OpportunityLineItem();
				OLI.OpportunityId = oppObjConverted[0].Id;
				OLI.PricebookEntryId = prbEId;
				OLI.Quantity = 1;
                OLI.Product2Id=productIdToAdd;
				OLI.UnitPrice = payFrequencyToCheck;
                try{
                    insert OLI;
                }
                catch(Exception e){
                    system.debug('Exception occured'+e.getMessage());
                }
				convertOppUpdate = [Select id, StageName from Opportunity where id =: oppObjConverted[0].Id Limit 1];
				if (convertOppUpdate.size() > 0 ) {
					convertOppUpdate[0].StageName = 'Closed Won';
                    convertOppUpdate[0].LeadSource='Website';
                    convertOppUpdate[0].JPN_Payment_Method__c='Credit Card';
					convertOppUpdate[0].CORE_Payment_Frequency__c = payFrequency;
					convertOppUpdate[0].Token_Id__c = tokenIdInRequest;
					convertOppUpdate[0].Payment_Status__c = paymentIdInRequest;
					if (locList.size() > 0) {
						convertOppUpdate[0].CORE_Location__c = locList[0].id;
					}
					closeWonList.add(convertOppUpdate[0]);
					if (closeWonList.size() > 0) {
                        try{
                            update closeWonList;
                        }
                        catch(exception e){
                            system.debug('Exception occured'+e.getMessage());
                        }
						
					}
				}
			}
		}
	}
	public static void handleConvertedLeadCrossSell(string leadidinSalesforce, string prbEId, Decimal payFrequencyToCheck, string payFrequency, string tokenIdInRequest, string paymentIdInRequest,string productIdToAdd, String paymentMethod) {
		List < Lead > lObjConverted = new List < Lead > ();
		List < Opportunity > oppObjConverted = new List < Opportunity > ();
		List < Schema.Location > locList = new List < Schema.Location > ();
		List < Opportunity > convertOppUpdate = new List < Opportunity > ();
		List < Opportunity > closeWonList = new List < Opportunity > ();
		lObjConverted = [Select id, name, ConvertedContactId, ConvertedopportunityId, ConvertedAccountId from Lead where isconverted = true and id =: leadidinSalesforce];
		If(lObjConverted.size() > 0) {
            system.debug('Account id'+lObjConverted[0].ConvertedAccountId);
			oppObjConverted = [Select id, Name from Opportunity where id =: lObjConverted[0].ConvertedopportunityId];
			locList = [Select Id,CORE_Account__c FROM location where CORE_Account__c =: lObjConverted[0].ConvertedAccountId Limit 1];
            system.debug('locList'+locList);
            
			if (oppObjConverted.size() > 0) {
				OpportunityLineItem OLI = new OpportunityLineItem();
				OLI.OpportunityId = oppObjConverted[0].Id;
				OLI.PricebookEntryId = prbEId;
				OLI.Quantity = 1;
                OLI.Product2Id=productIdToAdd;
				OLI.UnitPrice = payFrequencyToCheck;
                try{
                    insert OLI;
                }
                catch(Exception e){
                    system.debug('Exception occured'+e.getMessage());
                }
				convertOppUpdate = [Select id, StageName from Opportunity where id =: oppObjConverted[0].Id Limit 1];
				if (convertOppUpdate.size() > 0 ) {
					convertOppUpdate[0].StageName = 'Closed Won';
                    convertOppUpdate[0].LeadSource='Website';
                    convertOppUpdate[0].JPN_Payment_Method__c= paymentMethod;
					convertOppUpdate[0].CORE_Payment_Frequency__c = payFrequency;
					convertOppUpdate[0].Token_Id__c = tokenIdInRequest;
					convertOppUpdate[0].Payment_Status__c = paymentIdInRequest;
					if (locList.size() > 0) {
						convertOppUpdate[0].CORE_Location__c = locList[0].id;
					}
					closeWonList.add(convertOppUpdate[0]);
					if (closeWonList.size() > 0) {
                        try{
                            update closeWonList;
                        }
                        catch(exception e){
                            system.debug('Exception occured'+e.getMessage());
                        }
						
					}
				}
			}
		}
	}
	public static void handleListOfConvertedLeads(List < id > leadIdList, Map < id, string > OpptyIdProdCodeMap, Map < string, id > mapOfProductCodePriceBookId, Map < string, Decimal > mapofProductCodeProductPriceFromCampMap, string payFrequency, string tokenIdInRequest, string paymentIdInRequest, String existingAccountId,Map < string, id > prodCodeProdIdMap) {
		List < Lead > lObjConverted1 = new List < Lead > ();
		List < id > oppConvertedIds1 = new List < id > ();
		List < Opportunity > oppObjConverted1 = new List < Opportunity > ();
		List < Schema.Location > locList1 = new List < Schema.Location > ();
		List < Opportunity > closeWonList1 = new List < Opportunity > ();
		List < OpportunityLineItem > oppLineItemList1 = new List < OpportunityLineItem > ();
		string prodCodeRelatedToOppId;
		string pricebookEntIdRelatedToProdCode;
        system.debug('leadIdList'+leadIdList);
        system.debug('leadIdList size'+leadIdList.size());
        system.debug('OpptyIdProdCodeMap'+OpptyIdProdCodeMap);
        system.debug('mapOfProductCodePriceBookId'+mapOfProductCodePriceBookId);
        system.debug('mapofProductCodeProductPriceFromCampMap'+mapofProductCodeProductPriceFromCampMap);
        system.debug('mapofProductCodeProductPriceFromCampMap'+mapofProductCodeProductPriceFromCampMap);
        
        
		locList1 = [Select Id FROM location where CORE_Account__c =: existingAccountId Limit 1];
		lObjConverted1 = [Select id, name, ConvertedContactId, ConvertedopportunityId, ConvertedAccountId, ProductCodeRelatedtoCampaignCode__c, CampaignCodeOnLead__c from Lead where isconverted = true and id in: leadIdList];
		for (Lead l: lObjConverted1) {
			oppConvertedIds1.add(l.ConvertedopportunityId);
		}
		if (oppConvertedIds1.size() > 0) {
			oppObjConverted1 = [Select id, Name from Opportunity where id in: oppConvertedIds1];
            
			for (opportunity opp: oppObjConverted1) {
                system.debug('oppId'+opp.id);
				OpportunityLineItem OLI = new OpportunityLineItem();
				OLI.OpportunityId = opp.Id;
				prodCodeRelatedToOppId = OpptyIdProdCodeMap.get(opp.id);
                system.debug('prodCodeRelatedToOppId'+prodCodeRelatedToOppId);
				pricebookEntIdRelatedToProdCode = mapOfProductCodePriceBookId.get(prodCodeRelatedToOppId);
				OLI.PricebookEntryId = pricebookEntIdRelatedToProdCode;
				OLI.Quantity = 1;
                OLI.Product2Id=prodCodeProdIdMap.get(prodCodeRelatedToOppId);
				OLI.UnitPrice = mapofProductCodeProductPriceFromCampMap.get(prodCodeRelatedToOppId);
				oppLineItemList1.add(OLI);
			}
			if (oppLineItemList1.size() > 0) {
                try{
                    insert oppLineItemList1;
                }
                catch(Exception e){
                    system.debug('Exception occured'+e.getMessage());
                }
			}
			for (opportunity oppObj: oppObjConverted1) {
				oppObj.StageName = 'Closed Won';
                oppObj.LeadSource='Website';
                oppObj.JPN_Payment_Method__c='Credit Card';
				oppObj.CORE_Payment_Frequency__c = payFrequency;
				oppObj.Token_Id__c = tokenIdInRequest;
				oppObj.Payment_Status__c = paymentIdInRequest;
				if (locList1.size() > 0) {
					oppObj.CORE_Location__c = locList1[0].id;
				}
				closeWonList1.add(oppObj);
			}
			if (closeWonList1.size() > 0) {
                try{
                    update closeWonList1;
                }
                catch(Exception e){
                    system.debug('Exception occured'+e.getMessage());
                }
			}
		}
	}
    public static void handleConvertedLeadForServiceContractFile(string leadidinSalesforce, string prbEId, Decimal payFrequencyToCheck, string payFrequency, string tokenIdInRequest, string paymentIdInRequest,string productIdToAdd) {
		List < Lead > lObjConverted = new List < Lead > ();
        List<servicecontract> scList=new List<servicecontract>();
        Set<Id> scContractId=new Set<Id>();
		List < Opportunity > oppObjConverted = new List < Opportunity > ();
		List < Schema.Location > locList = new List < Schema.Location > ();
		List < Opportunity > convertOppUpdate = new List < Opportunity > ();
		List < Opportunity > closeWonList = new List < Opportunity > ();
		lObjConverted = [Select id, name, ConvertedContactId, ConvertedopportunityId, ConvertedAccountId from Lead where isconverted = true and id =: leadidinSalesforce];
		If(lObjConverted.size() > 0) {
            system.debug('Account id'+lObjConverted[0].ConvertedAccountId);
			oppObjConverted = [Select id, Name from Opportunity where id =: lObjConverted[0].ConvertedopportunityId];
			locList = [Select Id,CORE_Account__c FROM location where CORE_Account__c =: lObjConverted[0].ConvertedAccountId Limit 1];
            system.debug('locList'+locList);
            
			if (oppObjConverted.size() > 0) {
				OpportunityLineItem OLI = new OpportunityLineItem();
				OLI.OpportunityId = oppObjConverted[0].Id;
				OLI.PricebookEntryId = prbEId;
				OLI.Quantity = 1;
                OLI.Product2Id=productIdToAdd;
				OLI.UnitPrice = payFrequencyToCheck;
                try{
                    insert OLI;
                }
                catch(Exception e){
                    system.debug('Exception occured'+e.getMessage());
                }
				
				convertOppUpdate = [Select id, StageName from Opportunity where id =: oppObjConverted[0].Id Limit 1];
				if (convertOppUpdate.size() > 0) {
					convertOppUpdate[0].StageName = 'Closed Won';
                    convertOppUpdate[0].LeadSource='Website';
                    convertOppUpdate[0].JPN_Payment_Method__c='Credit Card';
					convertOppUpdate[0].CORE_Payment_Frequency__c = payFrequency;
					convertOppUpdate[0].Token_Id__c = tokenIdInRequest;
					convertOppUpdate[0].Payment_Status__c = paymentIdInRequest;
					if (locList.size() > 0) {
						convertOppUpdate[0].CORE_Location__c = locList[0].id;
					}
					closeWonList.add(convertOppUpdate[0]);
					if (closeWonList.size() > 0) {
                        try{
                            update closeWonList;
                        }
                        catch(Exception e){
                            system.debug('Exception occured'+e.getMessage());
                        }
                        //scList=[Select id from Servicecontract where Opportunity__c=:oppObjConverted[0].Id];
                        //for(servicecontract sc:scList)
                        //{
                           // scContractId.add(sc.id);
                        //}
                       // CORE_GenerateDocument_Handler.execute1(scContractId);
					}
				}
			}
		}
	}
    public static void handleListOfConvertedLeadsForServiceContractFile(List < id > leadIdList, Map < id, string > OpptyIdProdCodeMap, Map < string, id > mapOfProductCodePriceBookId, Map < string, Decimal > mapofProductCodeProductPriceFromCampMap, string payFrequency, string tokenIdInRequest, string paymentIdInRequest, String existingAccountId,Map < string, id > prodCodeProdIdMap) {
		List < Lead > lObjConverted1 = new List < Lead > ();
		List < id > oppConvertedIds1 = new List < id > ();
        List<servicecontract> scList=new List<servicecontract>();
        Set<Id> scContractId=new Set<Id>();
		List < Opportunity > oppObjConverted1 = new List < Opportunity > ();
		List < Schema.Location > locList1 = new List < Schema.Location > ();
		List < Opportunity > closeWonList1 = new List < Opportunity > ();
		List < OpportunityLineItem > oppLineItemList1 = new List < OpportunityLineItem > ();
		string prodCodeRelatedToOppId;
		string pricebookEntIdRelatedToProdCode;
        system.debug('leadIdList'+leadIdList);
        system.debug('leadIdList size'+leadIdList.size());
        system.debug('OpptyIdProdCodeMap'+OpptyIdProdCodeMap);
        system.debug('mapOfProductCodePriceBookId'+mapOfProductCodePriceBookId);
        system.debug('mapofProductCodeProductPriceFromCampMap'+mapofProductCodeProductPriceFromCampMap);
        system.debug('mapofProductCodeProductPriceFromCampMap'+mapofProductCodeProductPriceFromCampMap);
        
        
		locList1 = [Select Id FROM location where CORE_Account__c =: existingAccountId Limit 1];
		lObjConverted1 = [Select id, name, ConvertedContactId, ConvertedopportunityId, ConvertedAccountId, ProductCodeRelatedtoCampaignCode__c, CampaignCodeOnLead__c from Lead where isconverted = true and id in: leadIdList];
		for (Lead l: lObjConverted1) {
			oppConvertedIds1.add(l.ConvertedopportunityId);
		}
		if (oppConvertedIds1.size() > 0) {
			oppObjConverted1 = [Select id, Name from Opportunity where id in: oppConvertedIds1];
			for (opportunity opp: oppObjConverted1) {
				OpportunityLineItem OLI = new OpportunityLineItem();
				OLI.OpportunityId = opp.Id;
				prodCodeRelatedToOppId = OpptyIdProdCodeMap.get(opp.id);
				pricebookEntIdRelatedToProdCode = mapOfProductCodePriceBookId.get(prodCodeRelatedToOppId);
				OLI.PricebookEntryId = pricebookEntIdRelatedToProdCode;
				OLI.Quantity = 1;
                OLI.Product2Id=prodCodeProdIdMap.get(prodCodeRelatedToOppId);
				OLI.UnitPrice = mapofProductCodeProductPriceFromCampMap.get(prodCodeRelatedToOppId);
				oppLineItemList1.add(OLI);
			}
			if (oppLineItemList1.size() > 0) {
                try{
                    insert oppLineItemList1;
                }
                catch(Exception e){
                    system.debug('Exception occured'+e.getMessage());
                }
			}
			for (opportunity oppObj: oppObjConverted1) {
				oppObj.StageName = 'Closed Won';
                oppObj.LeadSource='Website';
                oppObj.JPN_Payment_Method__c='Credit Card';
				oppObj.CORE_Payment_Frequency__c = payFrequency;
				oppObj.Token_Id__c = tokenIdInRequest;
				oppObj.Payment_Status__c = paymentIdInRequest;
				if (locList1.size() > 0) {
					oppObj.CORE_Location__c = locList1[0].id;
				}
				closeWonList1.add(oppObj);
			}
			if (closeWonList1.size() > 0) {
                try{
                    update closeWonList1;
                }
                catch(Exception e){
                    system.debug('Exception occured'+e.getMessage());
                }
               // scList=[Select id from Servicecontract where Opportunity__c in:oppConvertedIds1];
                       // for(servicecontract sc:scList)
                        //{
                         //   scContractId.add(sc.id);
                        //}
                        //CORE_GenerateDocument_Handler.execute1(scContractId);
			}
		}
	}
   
}