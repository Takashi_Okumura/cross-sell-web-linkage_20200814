/*
** Class Name: Core_AccountInfoDetail
** Description: Web service class to return Lead Data.
** Version: 1.0
** Created By: Keizu Y.Tsuruta
** Modified By: 
*/

@RestResource(urlMapping = '/AccountInfoDetail/*')
global with sharing class Core_AccountInfoDetail {
    //Global Method: To send the Account data.
    @Httpget
    global static void getAccountDetail() {
        //---- Step-By-Step approch ------
        //1. Check if Request is null 
        //2. Extract the Lead code received in the Request
        //3. Query the lead
        //4. Return Success
        //5. Return Failure            
                
        AccountWrapper objWrapper = new AccountWrapper();
        List<Lead> leadList= new List<Lead>();
        List<Account> accountList= new List<Account>();
        List<Account> accWorkList= new List<Account>();
        List<CampaignMember> cmpList= new List<CampaignMember>();
        List<ServiceContract> svcList= new List<ServiceContract>();
        List<Contact> conWorkList= new List<Contact>();
        List<Opportunity> oppWorkList= new List<Opportunity>();
        string response;
        RestRequest request = RestContext.Request;
        RestResponse restResponse = RestContext.response;
        JsonReplaceStruct jsonStruct = new JsonReplaceStruct();

        try {
            
            String leadCode;
            boolean emailFlg; 
            boolean mailFlg;
            boolean bothFlg;
            Boolean existsAcc = true;
            Boolean existsCmp = true;
            Boolean existsSvc = true;
            Boolean existsCon = true;
            Boolean existsOpp = true;

            string message;
            //1. Check if Request is null 
            if (request != null && request.requestURI != null && request.requestURI != '') {
                //2. Extract the Lead code received in the Request
                leadCode = request.requestURI.substring(request.requestURI.lastIndexOf('/') + 1);
                
                //3. Query the lead
                leadList = [SELECT  Id, JPN_CustomerNumber__c 
                            FROM Lead 
                            WHERE Lead_Code__c=:leadCode AND IsConverted = False
                            Order by LastModifiedDate desc limit 1];
                //4. Return Success
                if(leadList.size() > 0 && leadList[0].JPN_CustomerNumber__c != null)
                {
                    //Fetching the account
                    accountList = [SELECT Id
                                         ,Name
                                         ,JPN_nameKana__c
                                         ,BillingPostalCode
                                         ,BillingState
                                         ,BillingCity
                                         ,BillingStreet
                                         ,Phone
                                         ,CORE_email__c
                                         ,Date_of_Birth__c
                                   FROM Account 
                                   WHERE JPN_AccountNumber__c =: leadList[0].JPN_CustomerNumber__c];
                    if(accountList.size() > 0){

                        accWorkList = [SELECT JPN_Email__c, JPN_mailCheckBox__c, JPN_both__c FROM Account
                                          WHERE JPN_AccountNumber__c =: leadList[0].JPN_CustomerNumber__c];
                        for(Account acc : accWorkList){
                            emailFlg = acc.JPN_Email__c;
                            mailFlg = acc.JPN_mailCheckBox__c;
                            bothFlg = acc.JPN_both__c;
                            break;
                        }

                        for(Account acc : accountList){
                            system.debug('account**' + acc);
                            jsonStruct.accountId = acc.Id;
                            jsonStruct.sendMail = getSendMethod(emailFlg, mailFlg, bothFlg);
                            jsonStruct.Phone = acc.Phone;
                            jsonStruct.emailBef = acc.CORE_email__c;
                            if(acc.Date_of_Birth__c != null){
                                jsonStruct.dateofBirthBef = ((DateTime)acc.Date_of_Birth__c).format('yyyy-MM-dd');
                                jsonStruct.dateofBirthAft = jsonStruct.dateofBirthBef.replace('-','/');    
                            }
                            objWrapper.objAccount = acc;
                            break;
                        }
                    } else {
                        existsAcc = false;
                    }

                    //Fetching the campaign related to the lead
                    cmpList = [SELECT Campaign.CORE_CampaignCode__c 
                               FROM CampaignMember 
                               WHERE LeadId =: leadList[0].id
                               Order by LastModifiedDate desc limit 1];

                    if(cmpList.size() > 0){
                        for(CampaignMember campMemb : cmpList){
                            system.debug('campMemb**' + campMemb);
                            objWrapper.CORE_CampaignCode = campMemb.Campaign.CORE_CampaignCode__c;
                            break;
                        }
                    } else {
                        existsCmp = false;
                    }
                                               
                    //Fetching the ServiceContract related to the Account
                    svcList = [SELECT Id
                                     ,CORE_Payment_Method__c
                                     ,JPN_CustomerID__c 
                               FROM ServiceContract 
                               WHERE AccountId =: jsonStruct.accountId
                               Order by CreatedDate asc limit 1];
                    if(svcList.size() > 0){
                        for(ServiceContract service : svcList){
                            system.debug('ServiceContract**' + service);
                            jsonStruct.serviceContractId = service.Id;
                            jsonStruct.paymentMethodBef = service.CORE_Payment_Method__c;
                            jsonStruct.paymentMethodAft = getPaymentMethod(service.CORE_Payment_Method__c);
                            objWrapper.objServiceContract = service;
                            break;
                        }
                    } else {
                        existsSvc = false;
                    }

                    //Fetching the Contact related to the Account
                    conWorkList = [SELECT Id
                                      ,Email
                                FROM Contact 
                                WHERE AccountId =: jsonStruct.accountId
                                Order by LastModifiedDate desc limit 1];
                    if(svcList.size() > 0){
                        for(Contact con : conWorkList){
                            system.debug('Contact**' + con);
                            jsonStruct.emailAft = con.Email;
                            break;
                        }
                    } else {
                        existsCon = false;
                    }

                    //Fetching the Opportunity related to the Account
                    oppWorkList = [SELECT Id
                                      ,CORE_Payment_Frequency__c
                                FROM Opportunity 
                                WHERE AccountId =: jsonStruct.accountId
                                Order by CreatedDate desc limit 1];
                    if(svcList.size() > 0){
                        for(Opportunity opp : oppWorkList){
                            system.debug('Opportunity**' + opp);
                            jsonStruct.paymentfrequency = opp.CORE_Payment_Frequency__c;
                            break;
                        }
                    } else {
                        existsOpp = false;
                    }
                    
                    restResponse.statusCode=200;
                    if(!existsAcc || !existsCmp || !existsSvc || !existsCon || existsOpp) message = '';
                    if(!existsAcc) message = 'Account Not Found. ';
                    if(!existsCmp) message += 'CampaignMember Not Found. ';
                    if(!existsSvc) message += 'ServiceContract Not Found. ';
                    if(!existsCon) message += 'Contact Not Found. ';
                    if(!existsOpp) message += 'Opportunity Not Found. ';
                    objWrapper.message = message;
                    restResponse.responseBody=Blob.valueOf(replaceJsonToSend(JSON.Serialize(objWrapper), jsonStruct));
                    CORE_IntegrationLogHandler.webserviceResponseCatcher(request,JSON.Serialize(objWrapper),'AccountInfoDetail');
                }
                //5. Return Failure 
                else
                {
                    restResponse.statusCode=404; 
                    objWrapper.message='Lead Not Found or CustomerNumber Not Set';
                    restResponse.responseBody=Blob.valueOf(replaceJsonToSend(JSON.Serialize(objWrapper), jsonStruct));
                    CORE_IntegrationLogHandler.webserviceResponseCatcher(request,JSON.Serialize(objWrapper),'AccountInfoDetail');
                }
                
            }
        } catch (Exception ex) {
            restResponse.statusCode=500; 
            objWrapper.message=ex.getMessage();
            restResponse.responseBody=Blob.valueOf(replaceJsonToSend(JSON.Serialize(objWrapper), jsonStruct));
            CORE_IntegrationLogHandler.webserviceResponseCatcher(request,JSON.Serialize(objWrapper),'AccountInfoDetail'); 
        }
 
    }
    
    private static string getSendMethod(Boolean email, Boolean mailCheckBox, Boolean both) {
        string sendMethod;
        if(both) {
            sendMethod = '両方';
        } else if(!both && email && mailCheckBox){
            sendMethod = '両方';            
        } else if(!both && email && !mailCheckBox){
            sendMethod = '電子メール';                        
        } else if(!both && !email && mailCheckBox){
            sendMethod = '郵送';                        
        } else if(!both && !email && !mailCheckBox){
            sendMethod = null;                                    
        }
        return sendMethod;
    }

    private static string getPaymentMethod(String payMethod) {
        string paymentMethod;
        if(payMethod == 'Credit Card'){
            paymentMethod = 'クレジットカード';
        } else if(payMethod == 'Direct Debit'){
            paymentMethod = '口座振替';
        } else {
            paymentMethod = null;                                    
        }
        return paymentMethod;
    }

    private static string replaceJsonToSend(string json, JsonReplaceStruct jsonStruct) {
        final String TAG_ID = '"Id":';
    	final String TAG_SEND_METHOD = '"SendMethod__c":';
    	final String TAG_DATE_OF_BIRTH = '"Date_of_Birth__c":';
    	final String TAG_PAYMENT_METHOD = '"CORE_Payment_Method__c":';
        final String TAG_EMAIL = '"CORE_email__c":';  
        final String TAG_PAYMENT_FREQUENCY = '"CORE_Payment_Frequency__c":';  
    	final String TAG_PHONE = '"Phone":';
        final String SEP_OBJ_SERVICE_CONTRACT = '"objServiceContract":';
        final String SEP_OBJ_ACCOUNT = '"objAccount":';
        final String SEP_MESSAGE = '"message":';  
        final String SEP_CAMPAIGN_CODE = '"CORE_CampaignCode__c":';  
        string tmpJsonBegin;
        string tmpJsonServiceContract;
        string tmpJsonAccount;
        string tmpJsonMessage;
        string tmpJsonCampaignCode;
        string tmpJsonEnd;
        string tmpJsonPhone;
        string tmpJsonEmail;
        string tmpJsonSendMethod;
        string tmpJsonDateOfBirth;
        string tmpJsonPaymentfrequency;

        json = json.replace('CORE_CampaignCode', 'CORE_CampaignCode__c');

        json = json.replace(',' + TAG_ID + '"' + jsonStruct.serviceContractId + '"', '');

        json = json.replace(',' + TAG_ID + '"' + jsonStruct.accountId + + '"', '');            

        if(jsonStruct.phone != null){
            tmpJsonPhone = TAG_PHONE + '"' + jsonStruct.phone + '"';
        } else {
            tmpJsonPhone = '';
        }

        if(jsonStruct.emailAft != null){
            tmpJsonEmail = ',' + TAG_EMAIL + '"' + jsonStruct.emailAft + '"';
        } else {
            tmpJsonEmail = '';
        }
        json = json.replace(',' + TAG_EMAIL + '"' + jsonStruct.emailBef + '"', '');

        if(jsonStruct.sendMail != null){
            tmpJsonSendMethod = ',' + TAG_SEND_METHOD + '"' + jsonStruct.sendMail + '"';
        } else {
            tmpJsonSendMethod = '';
        }

        if(jsonStruct.dateofBirthAft != null){
            tmpJsonDateOfBirth = ',' + TAG_DATE_OF_BIRTH + '"' + jsonStruct.dateofBirthAft + '"';
        } else {
            tmpJsonDateOfBirth = '';
        }
        json = json.replace(',' + TAG_DATE_OF_BIRTH + '"' + jsonStruct.dateofBirthBef + '"', ''); 

        if(jsonStruct.paymentfrequency != null){
            tmpJsonPaymentfrequency = ',' + TAG_PAYMENT_FREQUENCY + '"' + jsonStruct.paymentfrequency + '"';
        } else {
            tmpJsonPaymentfrequency = '';
        }

        json = json.replace(TAG_PHONE +'"' + jsonStruct.phone + '"' ,
               + tmpJsonPhone
               + tmpJsonEmail
               + tmpJsonSendMethod
               + tmpJsonDateOfBirth
               + tmpJsonPaymentfrequency
               );

        tmpJsonBegin = Json.substringBefore(SEP_OBJ_SERVICE_CONTRACT);
        tmpJsonServiceContract = ',' + SEP_OBJ_SERVICE_CONTRACT + json.substringBetween(SEP_OBJ_SERVICE_CONTRACT, SEP_OBJ_ACCOUNT);
        tmpJsonAccount = SEP_OBJ_ACCOUNT + json.substringBetween(SEP_OBJ_ACCOUNT, SEP_MESSAGE);
        tmpJsonMessage = SEP_MESSAGE + json.substringBetween(SEP_MESSAGE, ',' + SEP_CAMPAIGN_CODE);
        tmpJsonCampaignCode = SEP_CAMPAIGN_CODE + json.substringBetween(SEP_CAMPAIGN_CODE, '}');
        tmpJsonEnd = '}';
 
        system.debug('tmpJsonBegin**' + tmpJsonBegin);
        system.debug('tmpJsonServiceContract**' + tmpJsonServiceContract);
        system.debug('tmpJsonAccount**' + tmpJsonAccount);
        system.debug('tmpJsonMessage**' + tmpJsonMessage);
        system.debug('tmpJsonCampaignCode**' + tmpJsonCampaignCode);
        system.debug('tmpJsonEnd**' + tmpJsonEnd);

        json = tmpJsonBegin 
             + tmpJsonAccount 
             + tmpJsonCampaignCode 
             + tmpJsonServiceContract 
             + tmpJsonMessage
             + tmpJsonEnd;  

        return json;
    }

    //Wrapper class to prepare data
    global class AccountWrapper {
        global string          message            { get; set; }
        global Account         objAccount         { get; set; }
        global string          CORE_CampaignCode  { get; set; } 
        global ServiceContract objServiceContract { get; set; }
    }

    global class JsonReplaceStruct {
        string accountId;
        string serviceContractId;
        string sendMail;
        string paymentMethodBef;
        string paymentMethodAft;
        string dateofBirthBef;
        string dateofBirthAft;
        string emailBef;
        string emailAft;
        string paymentfrequency;
        string phone;
    }
}